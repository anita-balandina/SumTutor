function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function validateNumber(input, num, numId) {
    var inputNumber = $(input).val();
    if (num == inputNumber) {
        $(numId).css("background-color", "lightgreen");
        $(input).prop('disabled', true);
        $(input).css("background-color", "lightgreen");
        return true;
    }
    else {
        $(numId).css("background-color", "pink");
        $(input).css("background-color", "pink");
        return false;
    }
}

function validateFirstNumber(elem) {
    if (validateNumber(elem, a, "#a"))
        drawLineB();
}
function validateSecondNumber(elem) {
    if (validateNumber(elem, b, "#b"))
        getSum();
}

function validateSum(elem) {
    var sum = $("#inputSum").val();
    if (validateNumber(elem, a + b, "#result"))
        $("#result").html(a + b);
}

function getSum() {
    var end = 35 + (a + b) * 39;
    $("#inputs").append(inputTemplate({
        inputId: "inputSum",
        action: "validateSum(this)",
        leftMarginExp: "left:" + end + "px"
    }));

}

function drawLine(x1, x2) {
    $(document.createElementNS('http://www.w3.org/2000/svg', 'path')).attr({
        id: "line2",
        d: "M" + x1 + ",20 Q" + ((x1 + x2) / 2) + ",-15," + x2 + ",20",
        "marker-end": "url(#MarkerArrow)",
    }).appendTo("#arrow");
}

function drawLineB() {
    var x1 = 39 * a + 35;
    var x2 = 39 * b + x1;
    drawLine(x1, x2);
    $("#inputs").append(inputTemplate({
        inputId: "inputB",
        action: "validateSecondNumber(this)",
        leftMarginExp: "left:" + ((x1 + x2) / 2) + "px"
    }));

}

function drawLineA() {
    var x1 = 35;
    var x2 = 35 + (39 * a);
    drawLine(x1, x2);
    $("#inputs").append(inputTemplate({
        inputId: "inputA",
        action: "validateFirstNumber(this)",
        leftMarginExp: "left:" + ((x1 + x2) / 2 ) + "px"
    }));

}

function generateExample() {
    a = getRandomInt(6, 9);
    var c = getRandomInt(11, 14);
    b = c - a;
    $("#task").html(spanTemplate({valueA: a, valueB: b, valueRes: '?'}));

    drawLineA();
}


var spanTemplate = _.template("<span class='example-text'><var id='a'><%- valueA %></var>+<var id='b'><%- valueB %></var>=<var id='result'><%- valueRes %></var></span>")

var inputTemplate = _.template("<input class='form-control input-number' id=\"<%- inputId %>\" oninput=\"<%- action %>\"  style='<%= leftMarginExp %>'>");

var a, b;
